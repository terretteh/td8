public class Course {

    /**
     * Pré-requis : (à compléter)
     * Action : (à compléter)
     */
    public Course(Voiture uneVoit1, Voiture uneVoit2, int longueur) {
        throw new RuntimeException("Constructeur non implémenté ! Effacez cette ligne et écrivez le code nécessaire");
    }

    /**
     * Résultat : retourne une chaîne de caractères contenant les caractéristiques
     * de this (sous la forme de votre choix)
     */
    public String toString() {
        throw new RuntimeException("Méthode non implémentée ! Effacez cette ligne et écrivez le code nécessaire");
    }

    /**
     * Action : Simule le déroulement d’une course entre this.voit1 et this.voit2
     * sur une piste de longueur this.longueurPiste.
     * this.voit1 et this.voit2 sont d’abord placées sur la ligne de départ.
     * <p>
     * Ensuite, jusqu’à ce qu’une voiture franchisse la ligne d’arrivée, l’une
     * des deux voitures est itérativement sélectionnée aléatoirement et avance.
     * Un affichage des deux voitures (représentées par la première lettre de leur
     * nom) à leur position respective à chaque étape permet de suivre la course.
     * <p>
     * Résultat : la voiture gagnante.
     */
    public Voiture deroulement() {
        throw new RuntimeException("Méthode non implémentée ! Effacez cette ligne et écrivez le code nécessaire");
    }

}
